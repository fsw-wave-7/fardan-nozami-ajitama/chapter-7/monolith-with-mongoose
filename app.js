const express = require("express");
const mongoose = require("mongoose");
const MONGO_URI = "mongodb://localhost:27017/monolith";
const app = express();
const port = 3000;
const web = require("./routes/web");
const morgan = require("morgan");
const flash = require("connect-flash");

// Call library user session express
// Session store in memory browser
const session = require("express-session");

// Session save in mongodb
const MongoDBStore = require("connect-mongodb-session")(session);

// Create constants for open connection mongodb
const MONGODB_URI = "mongodb://localhost:27017/monolith";

// Set store session to mongodb and set for csrf feature
const store = new MongoDBStore({
  uri: MONGODB_URI,
  collection: "sessions",
});

// view engine ejs
app.set("view engine", "ejs");

// call asset from folder public
app.use(express.static("public"));

// parsing raw json
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(morgan("dev"));
app.use(
  session({
    secret: "secret",
    resave: false,
    saveUninitialized: false,
    store: store,
  })
);
app.use(flash());

app.use(web);
// 404 error handling
app.use((req, res) => {
  res.status(404).send("halaman tidak ditemukan");
});

// 500 error handling
app.use((err, req, res) => {
  res.status(500).send("internal server error");
});

mongoose
  .connect(MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  })
  .then(() => {
    app.listen(port, () => {
      console.log(`app running at http://localhost:${port}`);
    });
  })
  .catch((err) => console.log(err));
