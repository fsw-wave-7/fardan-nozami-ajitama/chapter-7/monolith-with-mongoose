const express = require("express");
const web = express.Router();
const WebController = require("../controllers/WebController");
const AuthController = require("../controllers/AuthController");
const webController = new WebController();
const authController = new AuthController();

// autj
web.get("/login", authController.getLogin);
web.post("/login", authController.postLogin);
web.get("/signup", authController.getSignUp);
web.post("/signup", authController.postSignUp);

// dashboard
web.get("/products", webController.getProducts);
web.get("/add-product", webController.getAddProduct);
web.post("/add-product", webController.postAddProduct);
module.exports = web;
