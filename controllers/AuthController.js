const User = require("../models/User");
const bcrypt = require("bcrypt");

class AuthController {
  getLogin = (req, res) => {
    let message = req.flash("error");
    if (message.length > 0) {
      message = message[0];
    } else {
      message = null;
    }
    res.render("index", {
      content: "./content/login",
      title: "Login",
      login: true,
      message,
    });
  };

  postLogin = (req, res) => {
    const { email, password } = req.body;
    User.findOne({
      email,
    })
      .then((user) => {
        if (!user) {
          req.flash("error", "user tidak terdaftar");
          return res.redirect("/login");
        }

        bcrypt
          .compare(password, user.password)
          .then((doMatch) => {
            if (doMatch) {
              req.session.isLoggedIn = true;
              req.session.user = user;
              return req.session.save((err) => {
                console.log(err);
                res.redirect("/");
              });
            }

            req.flash("error", "Password yang anda masukkan salah");
            res.redirect("/login");
          })
          .catch((err) => {
            console.log(err);
            res.redirect("/login");
          });
      })
      .catch((err) => console.log(err));
  };

  getSignUp = (req, res) => {
    let message = req.flash("error");
    if (message.length > 0) {
      message = message[0];
    } else {
      message = null;
    }
    res.render("index", {
      content: "./content/login",
      title: "Sign Up",
      login: false,
      message,
    });
  };

  postSignUp = (req, res) => {
    const { email, password, password2 } = req.body;
    const encryptPassword = bcrypt.hashSync(password, 12);
    User.findOne({
      email,
    })
      .then((user) => {
        if (user) {
          req.flash("error", "email sudah digunakan");
          return res.redirect("/signup");
        } else if (password !== password2) {
          req.flash("error", "password dan konfirmasi password tidak sama");
          return res.redirect("/signup");
        }
      })
      .then(() => {
        User.create({ email, password: encryptPassword });
        res.redirect("/login");
      });
  };
}

module.exports = AuthController;
