const Product = require("../models/Product");

class WebController {
  getProducts = (req, res) => {
    res.render("index", { content: "./content/listProducts" });
  };

  getAddProduct = (req, res) => {
    res.render("index", { content: "./content/addProduct" });
  };
  
  postAddProduct = (req, res) => {
    const { name, price, imageUrl } = req.body;
    console.log(req.body);

    Product.create({ name: name, price: price, imageUrl: imageUrl })
      .then(() => {
        res.redirect("/products");
      })
      .catch((err) => console.log(err));
  };

  editProduct = (req, res) => {};
  deleteProduct = (req, res) => {};
}

module.exports = WebController;
